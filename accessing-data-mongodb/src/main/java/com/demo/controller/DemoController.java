package com.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.demo.model.Demo;
import com.demo.repository.DemoRepository;

@RestController
public class DemoController {

  @Autowired
  private DemoRepository demoRepository;
  
  @PostMapping("/add")
  public String create(@RequestBody Demo book) {
    demoRepository.save(book);
    return "Added With Id: " + book.getId();
  }
  
  @GetMapping("/findAll")
  public List<Demo> getAll() {
    return demoRepository.findAll();
  }
  
  @GetMapping("/findAll/{id}")
  public Optional<Demo> getById(@PathVariable int id) {
    return demoRepository.findById(id);
  }
  
  @PutMapping("/update")
  public String update(@RequestBody Demo book) {
	  demoRepository.save(book);
	  return "Updated With Id: " + book.getId();
  }
  
  @DeleteMapping("/delete/{id}")
  public String delete(@PathVariable int id) {
    demoRepository.deleteById(id);
    return "Deleted With Id: " + id;
  }
}